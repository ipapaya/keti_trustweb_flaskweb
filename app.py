from flask import Flask, render_template, url_for, request
import json, os

app = Flask(__name__)

@app.route("/")
@app.route("/home")
def home():
    # template_name = request.args.get('template', default = '', type = str)
    jdata = request.args.get('jdata', default = '', type = str)
    print 'jdata: ' + jdata
    if jdata == '':
        jdata = 'output_normal1_181113.json'

    template_name = 'normal1.html'
    if template_name == '':
        template_name = 'about.html'
        return render_template(template_name)
    else:
        jsonDir = 'jsons/'
        path = os.path.abspath(jsonDir+jdata)
        print 'path: ' + path
        if not os.path.isfile(path):
            path = os.path.abspath(jsonDir+'output_normal1_181113.json')
        with open(path) as json_data:
            d = json.load(json_data)
            # print(d)

        print 'template_type: ' + str(d['template_type']) + ', ' + jdata

        if d['template_type'] == 1:
            template_name = '1_normal1.html'
        elif d['template_type'] == 2:
            template_name = '2_normal2.html'
        elif d['template_type'] == 3:
            template_name = '3_day.html'
        elif d['template_type'] == 4:
            template_name = '4_month.html'
        elif d['template_type'] == 5:
            template_name = '5_year.html'
        elif d['template_type'] == 11:
            template_name = '7_mix.html'
        elif d['template_type'] == 12:
            template_name = '7_mix0.html'
        elif d['template_type'] == 34:
            template_name = '7_mix.html'
        else:
            template_name = '1_normal1.html'

    print template_name
    return render_template(template_name, data=d)

@app.route("/t")
def template():
    template_name = request.args.get('template', default = '', type = str)
    jdata = request.args.get('jdata', default = '', type = str)
    jsonDir = 'jsons/'
    path = os.path.abspath(jsonDir+jdata)
    if not os.path.isfile(path):
        path = os.path.abspath(jsonDir+'output_normal1_181113.json')
    with open(path) as json_data:
        d = json.load(json_data)

    return render_template(template_name, data=d)

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0',port='8010')
